﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MiniProjectBatch229.Startup))]
namespace MiniProjectBatch229
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
